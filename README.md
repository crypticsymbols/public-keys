##Add Your Key

This assumes that you have read access to the git repository on Bitbucket.

1. `git clone git@bitbucket.org:commonmedia/ssh-public-keys.git`
2. `cd ssh-public-keys`
2. `cp ~/.ssh/id_rsa.pub keys/firstname_lastname.pub` (your key may not be named `id_rsa.pub` and your name may not be `Firstname Lastname`)
3. `git add .`
4. `git commit`
5. `git push origin master`

##Add A Key To A Server

This assumes that you're on the server, and that the user you're running this as has read access to the git repository on Bitbucket.

1. `cd ~/.ssh` or `cd /path/to/deployment/user/.ssh`
2. `git clone git@bitbucket.org:commonmedia/ssh-public-keys.git`
3. `cat ssh-public-keys/keys/firstname_lastname.pub >> authorized_keys` (the person you're adding might not be named `Firstname Lastname`)

##Refresh A Server

This assumes that you're on the server, and that the user you're running this as has read access to the git repository on Bitbucket.

1. `cd ~/.ssh` or `cd /path/to/deployment/user/.ssh`
2. `git clone git@bitbucket.org:commonmedia/ssh-public-keys.git`
3. Run `./catkeys.sh` to **REPLACE** the `authorized_keys` file with the keys in this repository

##Adding a user to only one server (contractor or client key)

1. As long as we don't commit and push to this repo from the server, we can just add non-CMI keys to the 'keys' folder as needed and run 'catkeys.sh' again to refresh the list.  Refreshing our keys from the repo won't wipe the untracked key.
